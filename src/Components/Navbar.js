import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import logo from '../logo.svg';
import ButtonContainer from './ButtonContainer';
import styled from 'styled-components';
//MUI
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';



class Navbar extends Component {
    render() {
        return (
            <NavWrapper className="navbar navbar-expand-sm   px-sm-5">
                <Link to="/">
                    <img src={logo} alt="store" className="navbar-brand"></img>
                </Link>
                <ul className="navbar-nav align-items-center">
                    <li className="nav-item ml-5"></li>
                    <Link to="/" className="nav-link">
                        Products
                    </Link>
                </ul>
                <Link to="/cart" className= "ml-auto">
                    <ButtonContainer>
                        <span className="mr-2"></span>
                        <ShoppingCartIcon />
                        My Cart
                    </ButtonContainer>
                </Link>
            </NavWrapper>
        )
    }
}

const NavWrapper = styled.nav`
background: var(--mainDark) !important;
.nav-link{
    color:var(--mainWhite) !important;
    font-size:1.3rem;
    text-transform: capitalize ;
}
`;



export default Navbar;
