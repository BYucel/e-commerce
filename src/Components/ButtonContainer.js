import styled from 'styled-components';


const ButtonContainer = styled.button`
text-transform: capatalize;
font-size:1.4rem;
background: transparent;
border:0.05rem solid var(--mainYellow);
color: ${prop => (prop.cart ? "var(--lightBlue)" : "var(--mainYellow)")};
border-radius:0.5rem;
border-color: ${props=> (props.cart ? "var(--lightBlue)":"var(--mainYellow)")};

padding:0.2rem 0.5rem;
cursor:pointer;
margin: 0.2rem 0.5rem 0.2rem  0;
transition: all 0.5s ease-in-out;
&:hover {
    background:${prop => (prop.cart ? "var(--lightBlue)" : "var(--mainYellow)")};
    color:${prop => (prop.cart ? "var(--mainWhite)" : "var(--mainDark)")};
}
&:focus{
    outline: none;
}
`;

export default ButtonContainer;

