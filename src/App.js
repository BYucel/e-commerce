import React from 'react';
import './App.css';
import {Switch, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './Components/Navbar';
import ProductList from './Components/ProductList';
import Cart from './Components/Cart/Cart';
import Details from './Components/Details';
import Default from './Components/Default';
import Modal from './Components/Modal';

//MUI
import {Home} from '@material-ui/icons';

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <Switch>
        <Route path="/details" component={Details} />
        <Route path="/cart" component={Cart} />
        <Route  exact path="/" component={ProductList} />
        <Route  component={Default} />
      </Switch>
      <Modal />
    </React.Fragment>
   
    
  );
}

export default App;
